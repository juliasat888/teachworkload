/** webpack.config.js */
const path = require('path');

const client = {
  entry: './src/index.js',
  target: 'web',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  devtool: 'inline-source-map',
  watch: true,
  mode: 'development',
  node: {
    net: 'empty',
    fs: "empty"
  },
  module: {
    rules: [ // Перечисляем правила, по которым будут выбираться лоадеры для тех или иных файлов
      {
        test: /\.jsx?$/, // Задаем регулярку, которая определит применять ли лоадер к файлу
        exclude: /(node_modules|bower_components)/, // Исключаем ненужные файлы
        use: { // Воспользуемся указанным ниже лоадером к указанными опциями (для babel)
          loader: 'babel-loader',
          options: {
            presets: ['env', 'react','stage-3'],
            plugins: ["transform-class-properties"]
          }
        }
      },
      {
        test: /\.css$/, // регулярка для файлов CSS
        use: ['style-loader', {
          loader: 'css-loader',
          options: { modules: true }
        }]
      }
    ]
  }
}
var nodeExternals = require('webpack-node-externals');
const server = {
  entry: './server.js',//'./bin/www.js',
  target: 'node',
  externals: [nodeExternals()],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'server.bundle.js'
  },
  devtool: 'inline-source-map',
  watch: true,
  mode: 'development',
  module: {
    rules: [ // Перечисляем правила, по которым будут выбираться лоадеры для тех или иных файлов
      {
        test: /\.jsx?$/, // Задаем регулярку, которая определит применять ли лоадер к файлу
        exclude: /(node_modules|bower_components)/, // Исключаем ненужные файлы
        use: { // Воспользуемся указанным ниже лоадером к указанными опциями (для babel)
          loader: 'babel-loader',
          
        }
      },
    ]
  }
}
module.exports = [client, server];
