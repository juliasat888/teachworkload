var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    res.locals.connection.connect()
   .then(pool=>{
    return pool.query `select distinct k.Код as code,p.ФИО as fio from dbo.ПреподавателиКафедры k
    inner join dbo.Преподаватели p
    on k.КодПреподавателя=p.Код
    where k.КодКафедры = 1 and  k.УчебныйГод = '2017-2018'`;
   })
   .then(result=>{
     res.send(JSON.stringify(result));
   })
   .catch(err=>{
    res.send({twtype_message:"error",message:"Сбой соединения с базой данных"});
   })
});

router.post('/diagram_year/*',function(req,res){
  console.log("post every diagram_year");
  console.log(req.body);
  res.locals.connection.connect()
   .then(pool=>{
     return pool.query `select p.ФИО,v.ВидЗанятийПолное,l.Нагрузка from dbo.НагрузкаПреподавателиЛето l
     join dbo.ПреподавателиКафедры k
     on l.КодДолжности = k.Код
     join dbo.Преподаватели p
     on p.Код = k.КодПреподавателя
     join dbo.НагрузкаВидЗанятий v
     on v.ВидЗанятий=l.ВидЗанятий
     where l.КодДолжности=${req.body.id} and l.УчебныйГод='2017-2018'`;
   })
   .then(result=>{
     console.log(result);
     res.send(JSON.stringify(result));
   })
   .catch(err=>{
    res.send({twtype_message:"error",message:"Сбой соединения с базой данных"});
   })
});


module.exports = router;