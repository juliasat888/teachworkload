var express = require('express');
var router = express.Router();
var sql = require("mssql");

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});
// define the home page route
router.get('/', function(req, res) {
  console.log("index");
  res.render('index');
});

router.get('/login',function(req, res){
  res.send('login');
});


var jwt = require ('jsonwebtoken'); 
var bcrypt = require ('bcryptjs'); 

router.post('/register', function (req, res){ 
  res.locals.connection.connect()
  .then(pool=>{
      let hashedPassword = bcrypt.hashSync(req.body.password, 8); 
      return   pool.request()
           .input('login', sql.NVarChar, req.body.login)
           .input('password', sql.NVarChar, hashedPassword)
           .input('twrole_id', sql.Int, 1)
           .input('second_name', sql.NVarChar, req.body.second_name)
           .input('first_name', sql.NVarChar, req.body.first_name)
           .input('patronymic', sql.NVarChar, req.body.patronymic)
           .input('email', sql.NVarChar, req.body.email)                    
           .output('code', sql.Int)
           .output('role', sql.Int)
           .execute('twservice.prc_twusers_insert');       
  })
  .then(result=>{
    if(result["output"]["code"] == -1)
      res.send({twtype_message:"error",message:"Пользователь с логином "+req.body.login+" уже существует. Выберите другой логин"});
    else res.send({code:result["output"]["code"],role:result["output"]["role"],twtype_message:"ok",message:"Пользователь с логином "+req.body.login+" успешно зарегистрирован в системе и автоматически авторизован"});
  })
  .catch(err=>{
    console.log(err);
    res.send({twtype_message:"error",message:"Сбой соединения с базой данных"});
  })
})

router.post('/login', function (req, res){ 
    res.locals.connection.connect()
    .then(pool=>{
        //проверить есть ли пользователь с таким логином в БД
        return pool.query `select password,twrole_id from twservice.twusers where login=${req.body.login}`;       
    })
    .then(result=>{
      console.log("result");
        let resJSON = JSON.stringify(result);
        console.log(resJSON);
        if(resJSON["recordset"]){
            console.log('login have records');
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
            var token = jwt.sign({ id: user._id }, process.env.secret, {
            expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token });
        }
        else{
            console.log('login not user');
            res.send({twtype_message:"error",message:"Пользователь не зарегистрирован"});
        }
    })
    .catch(err=>{
        console.log('login error connection');
        console.log(err);
        res.send({twtype_message:"error",message:"Сбой соединения с базой данных"});
    })
});

module.exports = router;
