
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var http = require('http');

require('dotenv').config(); //для работы с .env файлами
var dbConfig = {
  server: process.env.dbserver || 'localhost',
  database:process.env.db,
  user: process.env.dbuser,
  password: process.env.dbpassword,
  port: Number.parseInt(process.env.dbport) || 1433,
  options:{encrypt : false}
};

var app = express();
app.use(require('connect-livereload')());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname,'public')));

var mssql = require("mssql");
//Database connection
app.use(function(req, res, next){
  res.locals.connection = new mssql.ConnectionPool(dbConfig);
  next();
});

//здесь прописывать пути 
var index = require('./routes/index');
var comparison = require('./routes/comparison');
var every = require('./routes/every');

app.use('/comparison',comparison);
app.use('/every',every);
app.use('/', index);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var http = require('http');
//var port = normalizePort(process.env.PORT || '3000');
var port = 3000;
app.set('port', port);
var server = http.createServer(app);
server.listen(port);
//module.exports = app;