import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/App";
import style from './css/index.css';
import { BrowserRouter,Router, Route,Switch} from 'react-router-dom';


ReactDOM.render(
  <BrowserRouter>
    <App/> 
  </BrowserRouter>,
  document.getElementById('root')
);