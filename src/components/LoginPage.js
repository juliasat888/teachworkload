import React from 'react';
import { Link } from 'react-router-dom';
//import { connect } from 'react-redux';
//import { userActions } from '../actions';
import style from '../css/LoginPage.css';
export default class LoginPage extends React.Component {
    constructor(props){
        super();
        this.login = React.createRef();
        this.password = React.createRef();
    }
    handleOnClick = param => event =>{
        var data={"login":this.login.value,"password":this.password.value};
        fetch('/login', {
            method: 'POST',
            body:data
        }).then(function(response) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then(function(data) {
            console.log(data);
            alert(data.message);
        }).catch(err => {
            console.log('caught it!',err);
       })
    }
    render(){
        return(
            <div className="login-page">
                <div className="form">
                    <h1>Вход</h1>
                    <div className={style.login_page}>
                        <input ref={(input) => { this.login = input; }} type="text" placeholder="username"/>
                        <input ref={(input) => { this.password = input; }} type="password" placeholder="password"/>
                        <button className={style.button} onClick={this.handleOnClick(event)}>login</button>
                        <p className={style.message}>Not registered? <Link to="/register">Create an account</Link></p>
                    </div>
                </div>
            </div>
        );
    }
}