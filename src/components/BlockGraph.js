import React, { Component } from 'react';
import style from '../css/BlockAllTeachers.css';
import Chart from "../components/Chart";
import HighchartsExport from 'highcharts/modules/exporting';
import HighDiagram from "../components/highchart_diagram.js";

export default class BlockGraph extends Component{
    
    state = {
        config:{}
    }
    componentDidMount() {
        let self = this;
        console.log(this.props.location.pathname);
        if(this.props.location.pathname == '/comparison/diagram_year')
        {
            fetch('/comparison/diagram_year', {
                method: 'GET'
            }).then(function(response) {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            }).then(function(data) {
                var a = data["recordset"];
                var fio = HighDiagram.arrayForHighcharts("name",a);
                var val = HighDiagram.arrayForHighcharts("y",a);
                self.setState({config:HighDiagram.columnDiagram('Сравнительная нагрузка преподавателей','за учебный год','Нагрузка (часы)',fio,'Нагрузка',val)});
            }).catch(err => {
                console.log('Error! ',err);
            })
        }
        else if(new RegExp("/every/diagram_year/*").test(this.props.location.pathname)){
            console.log(this.props.match.params.id);
            fetch(this.props.location.pathname, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                body:JSON.stringify({id:this.props.match.params.id})
            }).then(function(response) {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            }).then(function(data) {
                console.log(data);
                var recordset = data["recordset"];
                var fio;
                if(recordset.length > 0)
                   fio = recordset[0]["ФИО"];
                var points = HighDiagram.pairCirclePoints("ВидЗанятийПолное","Нагрузка",recordset);
                console.log(points);
                console.log(HighDiagram.circleDiagram('Нагрузка преподавателя '+ fio,'за учебный год','Нагрузка',recordset));
                self.setState({config:HighDiagram.circleDiagram('Нагрузка преподавателя '+ fio,'за учебный год','Нагрузка',recordset)});
            }).catch(err => {
                console.log('Error!',err);
           })
        }
    }   
    render(){        
        return React.createElement(Chart, { container: 'chart', options: this.state.config, modules: [HighchartsExport] });
    }
}