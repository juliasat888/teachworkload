import React, { Component } from 'react';
import style from '../css/InputMenu.css';
import {Link} from 'react-router-dom';

export default class InputMenu extends Component{

    render(){
        return(
            <div className={style.inputMenu}>
                <div className={style.inputMenuContainer}>
                    <Link to="/login">Войти</Link>
                    <Link to="/register">Регистрация</Link>
                    <Link to="/">Главная страница</Link>
                </div>
            </div>
        );
    }
}