import React, { Component } from 'react';
import style from '../css/AdminBlock.css';

export default class AdminBlock extends Component{
    render(){
        return(
            <div>
            <table id="userRoleTable" className={style.adminTable}>
                <thead>
                <tr className={style.border}>
                    <th className={style.th}></th>
                    <th className={style.th}>Логин</th>
                    <th className={style.th}>Роль</th>
                    <th className={style.th}>Факультет</th>
                    <th className={style.th}>Кафедра</th>
                </tr>
                </thead>
                <tbody>
                    <tr className={style.border}>
                        <td className={style.border}><input type="checkbox"/></td>
                        <td className={style.border}>admin
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="1">зав.кафедры</option>
                                <option value="2">преподаватель</option>
                                <option value="3">администратор</option>
                            </select>
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="0">--</option>
                                <option value="1">ФЭВТ</option>
                            </select>
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="0">--</option>
                                <option value="1">ПОАС</option>
                                <option value="2">САПР</option>
                            </select>
                        </td>
                    </tr>
                    <tr className={style.border}>
                        <td className={style.border}><input type="checkbox"/></td>
                        <td className={style.border}>OrlovaJA
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="1">зав.кафедры</option>
                                <option value="2">преподаватель</option>
                                <option value="3">администратор</option>
                            </select>
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="0">--</option>
                                <option value="1">ФЭВТ</option>
                            </select>
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="0">--</option>
                                <option value="1">ПОАС</option>
                                <option value="2">САПР</option>
                            </select>
                        </td>
                    </tr>
                    <tr className={style.border}>
                        <td className={style.border}><input type="checkbox"/></td>
                        <td className={style.border}>SJulia
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="1">зав.кафедры</option>
                                <option value="2">преподаватель</option>
                                <option value="3">администратор</option>
                            </select>
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="0">--</option>
                                <option value="1">ФЭВТ</option>
                            </select>
                        </td>
                        <td className={style.border}>
                            <select className={style.select}>
                                <option value="0">--</option>
                                <option value="1">ПОАС</option>
                                <option value="2">САПР</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h1/>
            <div>
                <button className={style.button}>Добавить</button>
                <button className={style.button}>Сохранить</button>
                <button className={style.button}>Удалить</button>                
            </div>
            </div>
        );
    }
}