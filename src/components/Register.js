import React, { Component } from 'react';
import style from '../css/Register.css';
import { Link } from 'react-router-dom';
export default class Register extends Component{
    constructor(props){
        super();
        this.second_name = React.createRef();
        this.first_name = React.createRef();
        this.patronymic = React.createRef();
        this.email = React.createRef();
        this.login = React.createRef();
        this.password = React.createRef();
    }
    handleOnClick = param => event =>{
        fetch('/register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            body:JSON.stringify({
                second_name:this.second_name.value,
                first_name:this.first_name.value,
                patronymic:this.patronymic.value,
                email:this.email.value,
                login:this.login.value,
                password:this.password.value
            })
        }).then(function(response) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then(function(data) {
            console.log(data);
            alert(data.message);
        }).catch(err => {
            console.log('caught it!',err);
       })
    }
    render(){
        return(
        <div>
            <div className={style.container}>
                <h1>Регистрация</h1>
                <p>Заполните форму для регистрации в сервисе</p>
                <hr/>

                <label><b>Фамилия</b></label>
                <input type="text" placeholder="Фамилия" ref={(input) => {this.second_name = input; }} />

                <label><b>Имя</b></label>
                <input type="text" placeholder="Имя" ref={(input) => {this.first_name = input; }} />

                <label><b>Отчество</b></label>
                <input type="text" placeholder="Отчество" ref={(input) => {this.patronymic = input; }} />

                <label><b>Email</b></label>
                <input type="text" placeholder="Email" ref={(input) => {this.email = input; }} />

                <label><b>Логин</b></label>
                <input type="text" placeholder="Логин" ref={(input) => {this.login = input; }} />

                <label><b>Пароль</b></label>
                <input type="password" placeholder="Пароль" ref={(input) => {this.password = input; }} />

                <hr/>
                <button type="submit" className={style.registerbtn} onClick={this.handleOnClick(event)}>Register</button>
            </div>
        
            <div className="container signin">
                <p>Already have an account? <Link to="/login">Sign in</Link>.</p>
            </div>
        </div>
        );
    }
}