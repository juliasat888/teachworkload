exports.circleDiagram = function circleDiagram(
    pTitle,
    pSubtitle,
    pPointsName,
    pPoints){
    return({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: pTitle
        },
        subtitle: {
            text: pSubtitle
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y} ч</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: pPointsName,
            data: pPoints
        }]
    });
};

exports.columnDiagram = function(
    pTitle,
    pSubtitle,
    pyName,
    pCategories,    
    pSeriesName,
    pSeries
){
    return{
        title: {
            text: pTitle
        },
        subtitle: {
            text: pSubtitle
        },
        xAxis: {
            categories: pCategories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: pyName
            }
        },
        
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            type: 'column',
            name: pSeriesName,
            colorByPoint: true,
            data: pSeries,
            showInLegend: false
        }]
    };
}

exports.arrayForHighcharts = function (
    nameArray, //имена должны соответствовать тому, что прийдет из бд
    arrayValue //массив значений
){
    var resultArray = [];
    arrayValue.forEach(function(item, i, arr) {
        resultArray.push(item[nameArray]);
    });
    return resultArray;
}

exports.pairCirclePoints = function(
    nameStr,
    yStr,
    arrayValue
){
    var resultArray = [];
    var tmp;
    arrayValue.forEach(function(item, i, arr) {
        tmp = {};
        tmp["name"] = item[nameStr];
        tmp["y"] = item[yStr];
        resultArray.push(tmp);
    });
    return resultArray;
}
