import React, { Component } from 'react';
import InputMenu from "../components/InputMenu";
import SelectTypeGraph from "../components/SelectTypeGraph";
import style from '../css/App.css';
import { history } from '../helpers';

import AdminBlock from '../components/AdminBlock';
import UserBlock from '../components/UserBlock';
import { Router,Route,Link, Switch,Redirect,withRouter  } from 'react-router-dom';
import Register from "../components/Register";
import LoginPage from "../components/LoginPage";

var PrivateRoute  = require('../components/PrivateRoute');

export default class App extends Component{
    constructor(props){
        super(props);
        console.log(localStorage);
        localStorage.setItem('isAuthenticated',0)
        // this line is required to work on plunker because the app preview runs on a subfolder url
        history.push('/');
    }
    render(){
        return(
            <div className={style.app}>
                <div className={style.appHeaderContainer}>
                    <div className={style.appHeaderBlock}>
                        <header className={style.appHeader}>
                            <h1 className={style.appTitle}>TeachWorkload</h1>
                            <h3 >Сервис визуализации данных по нагрузке</h3>
                        </header>
                    </div>
                    <InputMenu/>
                </div>
                <Switch>                    
                    <Route path="/login" component={LoginPage}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/" component={UserBlock}/>
                </Switch>
            </div>
        );
    }
}
//<Route path="/*" component={UserBlock}/> <PrivateRoute path="/" component={UserBlock}/>