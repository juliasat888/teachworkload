import React, { Component } from 'react';
import {Route,Link, Switch } from 'react-router-dom';
import MenuSelectTypeGraph from '../components/MenuSelectTypeGraph';
import GraphicsAllTeachers from "../components/GraphicsAllTeachers";
import BlockGraph from '../components/BlockGraph';
import DiagramEveryTeachers from '../components/DiagramEveryTeachers';
import AuditoriumBlock from '../components/AuditoriumBlock';

export default class UserBlock extends Component{
    render(){
        console.log("userblock");
        return(
            <div>
                <MenuSelectTypeGraph/>                
                <Switch>
                 <Route exact path="/comparison" component={GraphicsAllTeachers}/>
                 <Route path="/comparison/" component={BlockGraph}/>                 
                 <Route path="/every/diagram_year/:id" component={BlockGraph}/>
                 <Route path="/every" component={DiagramEveryTeachers}/>
                 <Route path="/auditorium" component={AuditoriumBlock}/>
                </Switch>
            </div>
        );
    }
}