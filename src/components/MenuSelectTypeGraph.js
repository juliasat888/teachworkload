import React, { Component } from 'react';
import style from '../css/MenuSelectTypeGraph.css';
import GraphicsAllTeachers from "../components/GraphicsAllTeachers";
import { Router,Route,Link, Switch } from 'react-router-dom';

export default class MenuSelectTypeGraph extends Component{
    render(){
        console.log("MenuSelectTypeGraph");
        return(
            <ul className={style.ul}>
                <li className={style.li}><Link to="/comparison" className={style.a}>Сравнитeльная нагрузка преподавателей</Link></li>
                <li className={style.li}><Link to="/every" className={style.a}>Данные по нагрузке каждого преподавателя</Link></li>
                <li className={style.li}><Link to="/auditorium" className={style.a}>Нагрузка аудиторий</Link></li>
            </ul>
        );
    }
}