import React, { Component } from 'react';
import style from '../css/ListDiagram.css';
import { Router,Route,Link, Switch } from 'react-router-dom';
import style1 from '../css/DiagramEveryTeachers.css';

export default class DiagramEveryTeachers extends Component{
    constructor(props){
        super();
        this.state={selectOptions:[],selectedValue:0}
    }
    handleChange= param => event =>{
        this.setState({selectedValue : event.target.value});
    }
    componentDidMount() {
        let self =this;
        fetch('/every', {
            method: 'GET'
        }).then(function(response) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            console.log(response);
            return response.json();
        }).then(function(data) {
            let rows = [];
            let values = data["recordset"];
            console.log(values);
            values.forEach(function(item, i, arr) {
                rows.push(<option key={item["code"]} value={item["code"]}>{item["fio"]}</option>);
            });
            self.setState({selectOptions:rows});
            if(values.length > 0)
                self.setState({selectedValue:values[0]["code"]});
        }).catch(err => {
            console.log('Error! ',err);
        })
    }
    render(){
        console.log("DiagramEveryTeachers");        
        return(
            <div>
                <select className={style1.select} onChange={this.handleChange(event)}>
                    {this.state.selectOptions}
                </select>
                <ul className={style.list2a}>
                    <li><Link to={`/every/diagram_year/${this.state.selectedValue}`}>Нагрузка преподавателя по видам занятий за учебный год</Link></li> 
                </ul>   
            </div>         
        );
    }
}