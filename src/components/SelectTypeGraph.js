import React, { Component } from 'react';
import style from '../css/SelectTypeGraph.css';
import GraphicsAllTeachers from "../components/GraphicsAllTeachers";
import { Router,Route} from 'react-router-dom';

export default class SelectTypeGraph extends Component{
    state = {value : "all_teachers"}
    handleChange= param => event =>{
        this.setState({value : event.target.value});
    }
    render(){
        return(
            <div>
                <select className={style.selectType} value={this.state.value} onChange={this.handleChange(event)}>
                    <option value="all_teachers">Сравнитeльная нагрузка преподавателей</option>
                    <option value="each_teachers">Данные по нагрузке каждого преподавателя</option>
                    <option value="teachers">Нагрузка аудиторий</option>
                </select>
                <h1/>
                <div>
                    {
                        (
                            ()=>{
                                switch(this.state.value){
                                    case "all_teachers": console.log("1");
                                    return <GraphicsAllTeachers/>;//<BlockAllTeachers/>;
                                    break;
                                    case "each_teachers": console.log("2");
                                    return (//"2";
                                    <div>
                                        <p><a href="#">Общая нагрузка преподавателей за семестр</a></p>
                                        <p><a href="#">Общая нагрузка преподавателей за 2 недели</a></p>
                                    </div>
                                    );
                                    case "teachers": return "0";
                                    
                                }
                            }
                        )()
                    }
                </div>
            </div>
        );
    }
}