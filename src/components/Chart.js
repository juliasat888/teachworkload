import React, { Component } from 'react';
import Highcharts from 'highcharts';

export default class Chart extends Component{
    // When the DOM is ready, create the chart.
    componentDidMount() {
        console.log('componentDidMount_chart');
        // Extend Highcharts with modules
        if (this.props.modules) {
            this.props.modules.forEach(function (module) {
                module(Highcharts);
            });
        }
    }
    //Destroy chart before unmount.
    componentWillUnmount() {
        this.chart.destroy();
    }
    shouldComponentUpdate(nextProps, nextState){
        this.chart = new Highcharts[nextProps.type || "Chart"](
            nextProps.container, 
            nextProps.options
        );
        return true;
    }
    //Create the div which the chart will be rendered to.
    render() {
        return React.createElement('div', { id: this.props.container });
    }
}