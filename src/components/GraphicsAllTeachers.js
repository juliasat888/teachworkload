import React, { Component } from 'react';
import style from '../css/ListDiagram.css';
import { Router,Route,Link, Switch } from 'react-router-dom';

export default class GraphicsAllTeachers extends Component{
    render(){
        console.log("GraphicsAllTeachers");
        return(
            <ul className={style.list2a}>
                <li><Link to='/comparison/diagram_year'>Сравнительная нагрузка преподавателей за учебный год</Link></li> 
                <li><Link to='/comparison/diagram_year_lection'>Сравнительная нагрузка преподавателей за учебный год (лекции)</Link></li> 
                <li><Link to='/comparison/diagram_year_practic'>Сравнительная нагрузка преподавателей за учебный год (лекции)</Link></li> 
                <li><Link to='/comparison/diagram_1_sem'>Сравнительная нагрузка преподавателей за первый семестр учебного года</Link></li> 
                <li><Link to='/comparison/diagram_2_sem'>Сравнительная нагрузка преподавателей за второй семестр учебного года</Link></li>              
            </ul>            
        );
    }
}