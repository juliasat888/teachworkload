import React from 'react';
import { Route, Redirect } from 'react-router-dom';

var PrivateRoute = ({ component: Component, ...rest }) => (  
    <Route {...rest} render={(props) => (
      localStorage.isAuthenticated === true
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
module.exports = PrivateRoute;